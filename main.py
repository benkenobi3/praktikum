def hello(greeting: str, name: str):
    print(f'{greeting} {name}!')

if __name__ == '__main__':
    hello('git', 'conflicts')
